# vk-auth

Authorization in [Vkontakte](http://vk.com/) social network as [standalone/mobile](http://vk.com/dev/standalone) application. Allows you to get vk token with the help of PhantomJS headless browser.

##[Project moved to GitHub](https://github.com/DarkXaHTeP/vk-auth)

#### License: MIT